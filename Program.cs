﻿using System;
using System.Collections.Generic;

namespace FirstDuplicate
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[5]{2, 4, 3, 5, 1};
            int[] a1 = new int[6]{2, 1, 3, 5, 3, 2};

            System.Console.WriteLine(firstDuplicate(a));
            System.Console.WriteLine(firstDuplicate(a1));

        }

        static int firstDuplicate(int[] a) {
            var dictionary = new Dictionary<int, bool>();
            foreach (var item in a){
                if(dictionary.ContainsKey(item)){
                    return item;
                }else{
                    dictionary[item] = false;
                }
            }
            return -1;
        }

    }
}
